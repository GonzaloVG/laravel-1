<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Componente extends Model
{
    protected $tabla = 'componentes';
    protected $fillable = ['name', 'tipo', 'cantidad'];
}
