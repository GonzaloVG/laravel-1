<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('vista2');
});

//Ruta users

Route::get('/users', 'UsersController@index')->name('user-index');
Route::get('/users/create', 'UsersController@create')->name('user-create');
Route::post('/users/store', 'UsersController@store')->name('user-store');
Route::get('/users/show/{id}', 'UsersController@show')->name('user-show');
Route::get('users/edit/{id}', 'UsersController@edit')->name('user-edit');
Route::post('users/update/{id}', 'UsersController@update')->name('user-update');
Route::any('users/destroy/{id}', 'UsersController@destroy')->name('user-destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
