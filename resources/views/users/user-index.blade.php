<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>USERS</title>
</head>

<body>

    @foreach($usuarios as $u)
    <p>{{$u->id}}</p>
    <p>{{$u->name}}</p>
    <p>{{$u->email}}</p>
    <p><a href="{{ route('user-show', $u->id)}}">Visualizar</a></p>
    <p><a href="{{ route('user-edit', $u->id)}}">Editar</a></p>
    <p><a href="{{ route('user-destroy', $u->id)}}">Eliminar</a></p>
    <br>

    @endforeach

</body>

</html>