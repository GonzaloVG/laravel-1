<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form method="post" action="{{route('user-store')}}">

        <p>Se visualizan los datos de el usuario, pero no es para editar, si no para leer</p>

        {{csrf_field()}}
        <input type="text" name="name" class="form-control" value="{{$usuario->name}}" placeholder="nombre">
        <input type="email" name="email" class="form-control" value="{{$usuario->email}}" placeholder="email">
        <input type="password" name="password" class="form-control" placeholder="contraseña">
        <button type="submit">ENVIAR</button>


    </form>

</body>

</html>